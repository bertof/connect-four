\section{Ricerca}
Gli algoritmi di soluzione proposti in letteratura sono molteplici e variano di complessità~\cite{sarhan_real-time_2009}.
Quelli presi in considerazione sono Minimax~\cite{willem_minimax_1997} e Alpha-Beta Pruning~\cite{knuth_analysis_1975}, entrambi algoritmi di ricerca informata che sfruttano un'euristica per valutare uno stato di gioco e scelgono la prossima mossa simulandone il risultato e assumendo che l'avversario scelga sempre la mossa ottima.

\subsection{Minimax}
L'algoritmo di ricerca Minimax calcola il più alto punteggio che un giocatore otterrà con certezza dato un certo stato, senza conoscere a priori quali azioni gli altri giocatori sceglieranno.
Possiamo definire il valore cercato, chiamato valore \engl{maxmin}, come l'alternanza tra una operazione di massimizzazione e una di minimizzazione applicate al valore atteso di utilità di uno stato:
\begin{equation}
    \underline{v_i}=\max_{a_i}\min_{a_{-i}}v_i(a_i,a_{-i})
\end{equation}
dove \(i\) è l'indice del giocatore di interesse e \(-i\) l'avversario; \(a_i\) e \(a_{-i}\) rispettivamente le azioni intraprese dal giocatore \(i\) e \(i-1\); infine \(v_i\) è la funzione che restituisce il valore di utilità atteso.
Allo stesso modo possiamo definire il valore \engl{minimax}, come l'alternanza tra una operazione di minimizzazione e una di minimizzazione applicate al valore atteso di utilità di uno stato:
\begin{equation}
    \overline{v_i}=\min_{a_{-i}}\max_{a_i}v_i(a_i,a_{-i})
\end{equation}
Calcolare il valore \engl{maxmin} per un giocatore consiste nel trovare il migliore risultato garantito, seguendo tutti i possibili \engl{path} di gioco e assumendo che l'avversario cerchi sempre di scegliere l'azione ottima.
Possiamo, dunque, costruire un albero di ricerca i cui nodi sono gli stati di gioco e gli archi sono le azioni disponibili a partire da ciascuno stato.

È immediato dimostrare che l'algoritmo Minimax applicato ad un gioco a stati finiti, come Forza 4, è finito, ma la dimensione dell'albero di ricerca cresce molto velocemente, esponenzialmente con il numero di azioni disponibili, diventando presto ingestibile.
Per questo motivo, negli \engl{use case} reali, si utilizzano dei vincoli sulla profondità o sul tempo di ricerca, in tal modo è possibile limitare il numero di stati esplorati dell'albero, ma ottenere comunque una misura utile di quale percorso sia il più adatto per vincere.

\begin{algorithm}[H]
    \caption{Minimax}\label{algorithm:minimax}
    \begin{algorithmic}[1]
        \Function{minimax}{stato, profondita, giocatoreMassimizzante}
        \If{depth = 0 \OR{} il nodo è una foglia}
        \State{\Return{valore euristico dello stato}}
        \Else{}
        \If{giocatoreMassimizzante}
        \State{\(val:=-\infty \)}
        \ForAll{stato \(\in \) stati successivi}
        \State{\(val := \max(val, minimax(stato, profondita-1, False))\)}
        \EndFor{}
        \State{\Return{val}}
        \Else{}
        \State{\(val:=+\infty \)}
        \ForAll{stato \(\in \) stati successivi}
        \State{\(val := \min(val, minimax(stato, profondita-1, True))\)}
        \EndFor{}
        \State{\Return{val}}
        \EndIf{}
        \EndIf{}
        \EndFunction{}
    \end{algorithmic}
\end{algorithm}

\subsection{Alpha-Beta Pruning}
L'Alpha-Beta Pruning è un algoritmo di ricerca simile a Minimax che cerca di diminuire il numero di stati valutati durante la ricerca, fermando l'analisi di un certo percorso quando c'è sufficiente evidenza che esistano percorsi migliori.

L'algoritmo utilizza due valori, \(\alpha \) e \(\beta \), che rappresentano il minimo valore che il giocatore massimizzante può ottenere e il massimo valore che il giocatore minimizzante può raggiungere.
Quando si osserva che il massimo risultato ottenibile dal giocatore minimizzante diventa più piccolo del minimo risultato ottenibile dal giocatore massimizzante, il giocatore massimizzante può evitare di analizzare gli stati successivi nel \engl{path}, poiché è garantito che esiste un \engl{path} migliore.

Questa ottimizzazione risulta particolarmente utile se possiamo ordinare efficientemente la lista di esplorazione degli stati successivi in modo crescente sul valore atteso di utilità.
Nel caso di ordine ottimale dimostriamo che il numero atteso di stati esplorati è dimezzato, permettendo di esplorare un numero di stati utili doppio rispetto all'algoritmo Minimax.

\begin{algorithm}[H]
    \caption{Alpha-Beta Pruning}\label{algorithm:alpha-beta_pruning}
    \begin{algorithmic}[1]
        \Function{alpha\_beta}{stato, profondita, \(\alpha \), \(\beta \),giocatoreMassimizzante}
        \If{depth = 0 \OR{} il nodo è una foglia}
        \State{\Return{valore euristico dello stato}}
        \Else{}
        \If{giocatoreMassimizzante}
        \State{\(val:=-\infty \)}
        \ForAll{stato \(\in \) stati successivi}
        \State{\(val := \max(val, alpha\_beta(stato, profondita-1, False))\)}
        \State{\(\alpha := \max(\alpha, val)\)}
        \If{\(\alpha\geq\beta \)}
        \State{\BREAK}
        \EndIf{}
        \EndFor{}
        \State{\Return{val}}
        \Else{}
        \State{\(val:=+\infty \)}
        \ForAll{stato \(\in \) stati successivi}
        \State{\(val := \min(val, alpha\_beta(stato, profondita-1, True))\)}
        \State{\(\beta := \min(\beta, val)\)}
        \If{\(\alpha\geq\beta \)}
        \State{\BREAK}
        \EndIf{}
        \EndFor{}
        \State{\Return{val}}
        \EndIf{}
        \EndIf{}
        \EndFunction{}
    \end{algorithmic}
\end{algorithm}
