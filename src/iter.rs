use std::cmp::{max, min};
use std::iter::{once, repeat};

use crate::v2::V2;

/// Iterator over V2 values
///
/// Used to iterate over V2 coordinates horizontally, vertically or diagonally.
/// If the coordinates are not aligned it will reach the closest diagonal position between the origin and the target.
pub struct V2Iter {
    x_iter: Box<dyn Iterator<Item=i64>>,
    y_iter: Box<dyn Iterator<Item=i64>>,
}

impl V2Iter {

    /// Constructor
    pub fn new(origin: &V2<i64>, target: &V2<i64>) -> Self {
        if origin == target {
            let x_iter = Box::new(once(origin.x));
            let y_iter = Box::new(once(origin.y));
            return V2Iter { x_iter, y_iter };
        }

        let max_x = max(origin.x, target.x);
        let max_y = max(origin.y, target.y);
        let min_x = min(origin.x, target.x);
        let min_y = min(origin.y, target.y);

        let x_iter: Box<dyn Iterator<Item=i64>> =
            if origin.x == target.x {
                Box::new(repeat(origin.x))
            } else if origin.x < target.x {
                Box::new(min_x..=max_x)
            } else /*origin.x > target.x*/ {
                Box::new((min_x..=max_x).rev())
            };

        let y_iter: Box<dyn Iterator<Item=i64>> =
            if origin.y == target.y {
                Box::new(repeat(origin.y))
            } else if origin.y < target.y {
                Box::new(min_y..=max_y)
            } else /*origin.x > target.x*/ {
                Box::new((min_y..=max_y).rev())
            };

        V2Iter { x_iter, y_iter }
    }
}

impl Iterator for V2Iter {
    type Item = V2<i64>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(x) = self.x_iter.next() {
            if let Some(y) = self.y_iter.next() {
                Some(V2::new(x, y))
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn iter_single() {
        assert_eq!(
            vec![V2::new(1, 1)],
            V2Iter::new(&V2::new(1, 1), &V2::new(1, 1))
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn iter_row() {
        assert_eq!(
            vec![
                V2::new(-2, 0),
                V2::new(-1, 0),
                V2::new(0, 0),
                V2::new(1, 0),
                V2::new(2, 0)
            ],
            V2Iter::new(&V2::new(-2, 0), &V2::new(2, 0))
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn iter_diag_inc() {
        assert_eq!(
            vec![
                V2::new(-2, 2),
                V2::new(-1, 1),
                V2::new(0, 0),
                V2::new(1, -1),
                V2::new(2, -2)
            ],
            V2Iter::new(&V2::new(-2, 2), &V2::new(2, -2))
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn iter_diag_dec() {
        assert_eq!(
            vec![
                V2::new(2, -2),
                V2::new(1, -1),
                V2::new(0, 0),
                V2::new(-1, 1),
                V2::new(-2, 2)
            ],
            V2Iter::new(&V2::new(2, -2), &V2::new(-2, 2))
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn iter_diag_offset() {
        assert_eq!(
            vec![
                V2::new(-1, -1),
                V2::new(0, 0),
                V2::new(1, 1),
                V2::new(2, 2),
                V2::new(3, 3),
            ],
            V2Iter::new(&V2::new(-1, -1), &V2::new(3, 10))
                .collect::<Vec<_>>()
        );
    }
}