use std::{error, fmt};

/// Game error states
#[derive(Clone, Debug)]
pub enum Error {
    PositionOutOfGrid,
    FullColumn,
    GridAccess,
    NotCardinalAlignment,
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::PositionOutOfGrid => write!(f, "Position out of the grid"),
            Error::FullColumn => write!(f, "Column full"),
            Error::GridAccess => write!(f, "Error accessing grid"),
            Error::NotCardinalAlignment => write!(f, "Not a cardinal alignment"),
        }
    }
}