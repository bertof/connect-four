use std::fmt;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct V2<T> {
    pub x: T,
    pub y: T,
}

impl<T> V2<T> {
    pub fn new(x: T, y: T) -> Self {
        V2 { x, y }
    }
}

impl<T: fmt::Display> fmt::Display for V2<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "v2({},{})", self.x, self.y)
    }
}