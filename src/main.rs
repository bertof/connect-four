use std::error::Error;
use std::f64;

use clap::{App, Arg};
use piston::*;
use piston_window::*;
use rand::seq::IteratorRandom;
use rayon::prelude::*;

use logs::init_logger;
use search::{alpha_beta_pruning, Evaluation::*, minimax};
use state::{Color::*, State};
use v2::V2;

pub mod state;
pub mod search;
pub mod v2;
pub mod iter;
pub mod result;
pub mod error;
pub mod logs;

const CELL_SIZE: f64 = 65.0;
const WINDOW_MARGIN: f64 = 40.0;

fn main() -> Result<(), Box<dyn Error>> {
    init_logger();

    // CLI generation
    let matches = App::new("Connect 4")
        .version("1.0.0")
        .author("Filippo Berto <berto.f@protonmail.com>")
        .about("A Connect 4 game implementation in Rust")
        .arg(Arg::with_name("DEPTH")
            .short("d")
            .long("depth")
            .default_value("4"))
        .arg(Arg::with_name("ALGORITHM")
            .short("a")
            .long("algorithm")
            .default_value("alpha_beta"))
        .get_matches();

    // Arguments parsing
    let depth = matches
        .value_of("DEPTH")
        .and_then(|s| s.parse::<usize>().ok())
        .unwrap_or_else(|| {
            eprintln!("Invalid depth, using default 4");
            4
        });
    let algorithm = matches
        .value_of("ALGORITHM")
        .unwrap();

    // GUI window generation
    let opengl = OpenGL::V4_5;
    let mut window: PistonWindow =
        WindowSettings::new("Connect 4", [800, 600])
            .exit_on_esc(true)
            .graphics_api(opengl)
            .resizable(false)
            .build()
            .unwrap();

    let white = [1.0; 4];
    let black = [0.0, 0.0, 0.0, 1.0];

    let red = [1.0, 0.1, 0.0, 1.0];
    let blue = [0.0, 0.3, 1.0, 1.0];

    let assets = find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("assets").unwrap();
    let mut glyphs = window.load_font(assets.join("FiraSans-Regular.ttf")).unwrap();

    // Game state
    let mut state = State::new(&V2::new(8, 8));
    let mut mouse_position = [0.0; 2];

    // Event loop
    window.set_lazy(true); // Update events are disabled and rendering updates only on input
    while let Some(e) = window.next() {
        window.draw_2d(&e, |c, g, d| {

            // White background
            clear(white, g);

            // Draw grid
            let rect = [0.0, 0.0, CELL_SIZE, CELL_SIZE];
            for i in 0..8 {
                for j in 0..8 {
                    let c = c.trans(WINDOW_MARGIN + i as f64 * CELL_SIZE,
                                    WINDOW_MARGIN + j as f64 * CELL_SIZE);
                    rectangle(white, rect, c.transform, g);
                    Rectangle::new_border(black, 0.5)
                        .draw(rect, &c.draw_state, c.transform, g);
                }
            }

            // Print circles on each cell
            let rect = [2.5, 2.5, CELL_SIZE - 5.0, CELL_SIZE - 5.0];
            state.all_cells().iter().for_each(|v| {
                let color = state.cell_color(&v).unwrap().unwrap();
                let c = c.trans(WINDOW_MARGIN + v.x as f64 * CELL_SIZE,
                                WINDOW_MARGIN + CELL_SIZE * 7.0 - v.y as f64 * CELL_SIZE);
                ellipse(match color {
                    Red => red,
                    Blue => blue,
                }, rect, c.transform, g);
                Ellipse::new_border(black, 1.0)
                    .draw(rect, &c.draw_state, c.transform, g);
            });

            // Print winner text
            state.winner().map(|w| {
                let transform = c.transform.trans(
                    2.0 * WINDOW_MARGIN + 8.0 * CELL_SIZE,
                    2.0 * WINDOW_MARGIN);
                text::Text::new_color(black, 32).draw(
                    "Winner:",
                    &mut glyphs,
                    &c.draw_state,
                    transform, g,
                ).unwrap();
                let transform = c.transform.trans(
                    2.0 * WINDOW_MARGIN + 8.0 * CELL_SIZE,
                    2.0 * WINDOW_MARGIN + CELL_SIZE);
                text::Text::new_color(match w {
                    Red => red,
                    Blue => blue,
                }, 32).draw(
                    &format!("{:?}", w),
                    &mut glyphs,
                    &c.draw_state,
                    transform, g,
                ).unwrap();
            });

            // Update glyphs before rendering.
            glyphs.factory.encoder.flush(d);
        });

        // Mouse position tracking
        e.mouse_cursor_args().map(|pos| mouse_position = pos);

        // Mouse button handling
        e.button_args().map(|button_args| {
            if let Some(w) = state.winner() {
                println!("Winner: {:?}", w);
            } else {
                match &(button_args.button, button_args.state) {
                    (Button::Mouse(MouseButton::Left), ButtonState::Press) => {
                        if state.get_turn() == &Red {
                            // Human move
                            let x = f64::floor((mouse_position[0] - WINDOW_MARGIN) / 65.0) as i64;
                            if 0 <= x && x < state.get_sizes().x {
                                match state.next_state_x(x) {
                                    Ok(s) => state = s,
                                    Err(e) => eprintln!("Invalid move: {}", e),
                                }
                            }
                        }
                    }
                    (Button::Mouse(MouseButton::Left), ButtonState::Release) => {
                        if state.get_turn() == &Blue {
                            // Computer move
                            let mut next_moves = state
                                .valid_moves()
                                .par_iter()
                                .filter_map(|v| state
                                    .next_state_x(v.x)
                                    .map(|s| (v.clone(), s))
                                    .ok())
                                .filter_map(|(v, s)| match algorithm {
                                    "alpha_beta" =>
                                        alpha_beta_pruning(&s, &Blue,
                                                           Worst, Best, depth)
                                            .map(|r| (v, r)),
                                    "minimax" =>
                                        minimax(&s, &Blue, depth)
                                            .map(|r| (v, r)),
                                    _ => {
                                        eprintln!("Invalid algorithm: should use either alpha_beta or minimax");
                                        eprintln!("Using default alpha_beta");
                                        alpha_beta_pruning(&s, &Blue,
                                                           Worst, Best, depth)
                                            .map(|r| (v, r))
                                    }
                                })
                                .collect::<Vec<_>>();

                            {
                                // Print search results
                                next_moves.par_sort_by_key(|(v, _r)| v.x.clone());
                                println!("Results:");
                                next_moves.iter()
                                    .for_each(|(v, r)| println!("{} --> {:?}", v, r));
                            }

                            let max_v = next_moves.iter().max_by_key(|(_v, r)| r.clone());
                            max_v.map_or_else(|| {
                                println!("Can not move!");
                            }, |max_v| {
                                let mut rng = rand::thread_rng();

                                // Random choice on best moves
                                let (v, r) = next_moves
                                    .iter()
                                    .filter(|(_v, r)| r == &max_v.1)
                                    .choose(&mut rng)
                                    .unwrap();

                                println!("Next: {} -> {:?}", v, r);
                                match state.next_state_x(v.x) {
                                    Ok(s) => state = s,
                                    Err(e) => eprintln!("Invalid move: {}", e)
                                }
                            });
                        }
                    }
                    _ => ()
                }
            }
        });
    }

    Ok(())
}

