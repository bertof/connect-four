use std::cmp::{max, min};
use std::ops;

use rayon::prelude::*;

use crate::result;
use crate::search::Evaluation::*;
use crate::state::*;

/// Evaluation result
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum Evaluation {
    Worst,
    Val(i64),
    Best,
}

impl Evaluation {
    /// Negation of an evaluation result
    pub fn neg(&self) -> Self {
        match &self {
            Worst => Best,
            Best => Worst,
            Val(v) => Val(-v.clone())
        }
    }
}

/// Subtraction binary operator between Evaluations
impl ops::Sub for Evaluation {
    type Output = Evaluation;

    fn sub(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Best, _) => Best,
            (_, Worst) => Best,
            (Worst, _) => Worst,
            (_, Best) => Worst,
            (Val(v1), Val(v2)) => Val(v1 - v2)
        }
    }
}

/// State evaluation function
pub fn state_evaluation(state: &State, computer: &Color) -> result::Result<Evaluation> {
    let e1 = state.max_heuristic(&computer)?
        .map(|(_v, r)| r)
        .unwrap_or(Val(0));
    let e2 = state.max_heuristic(&computer.other())?
        .map(|(_v, r)| r)
        .unwrap_or(Val(0));
    Ok(e1 - e2)
}

/// Minimax search algorithm
pub fn minimax(state: &State, computer: &Color, depth: usize) -> Option<Evaluation> {
    if depth == 0 || state.valid_moves().len() == 0 || state.winner().is_some() {
        state_evaluation(&state, &computer).ok()
    } else {
        let next_states = state.valid_moves().par_iter()
            .filter_map(|v| state
                .next_state_x(v.x)
                .ok())
            .collect::<Vec<_>>();
        if state.get_turn() != computer {
            next_states.par_iter()
                .filter_map(|s|
                    minimax(&s, &computer, depth - 1))
                .min()
        } else {
            next_states.par_iter()
                .filter_map(|s|
                    minimax(&s, &computer, depth - 1))
                .max()
        }.clone()
    }
}

/// Alpha-Beta Pruning search algorithm
pub fn alpha_beta_pruning(state: &State, computer: &Color,
                          mut alpha: Evaluation, mut beta: Evaluation, depth: usize) -> Option<Evaluation> {
    if depth == 0 || state.valid_moves().len() == 0 || state.winner().is_some() {
        state_evaluation(&state, &computer).ok()
    } else {
        let next_states = state.valid_moves().par_iter()
            .filter_map(|v| state
                .next_state_x(v.x)
                .ok())
            .collect::<Vec<_>>();
        if state.get_turn() == computer {
            let mut res = Worst;
            for child in next_states {
                res = max(
                    res,
                    alpha_beta_pruning(&child, computer, alpha, beta, depth - 1)
                        .unwrap_or(Worst));
                alpha = max(alpha, res);
                if alpha >= beta {
                    break;
                }
            }
            Some(res)
        } else {
            let mut res = Best;
            for child in next_states {
                res = min(
                    res,
                    alpha_beta_pruning(&child, computer, alpha, beta, depth - 1)
                        .unwrap_or(Best));
                beta = min(beta, res);
                if alpha >= beta {
                    break;
                }
            }
            Some(res)
        }
    }
}
