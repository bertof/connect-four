/// Game error custom result definition
pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;
