use std::cmp::{max, min};
use std::fmt;

use rayon::prelude::*;

use crate::error::Error::*;
use crate::iter::V2Iter;
use crate::result;
use crate::search::Evaluation;
use crate::state::Color::*;
use crate::v2::V2;

/// Game state class
#[derive(Clone, Debug)]
pub struct State {
    /// Game grid as vector of columns of cells
    grid: Vec<Vec<Cell>>,
    /// Game grid dimensions
    sizes: V2<i64>,
    /// Current player turn
    turn: Color,
}

impl State {
    /// Game state constructor
    pub fn new(grid_size: &V2<i64>) -> Self {
        let grid = vec![Vec::new(); grid_size.x as usize];

        State {
            grid,
            sizes: grid_size.clone(),
            turn: Color::Red,
        }
    }

    /// Current player turn getter
    pub fn get_turn(&self) -> &Color {
        &self.turn
    }

    /// Game grid dimension getter
    pub fn get_sizes(&self) -> &V2<i64> {
        &self.sizes
    }

    /// Check for a coordinate to be inside the grid
    pub fn check_grid_bounds(&self, pos: &V2<i64>) -> result::Result<()> {
        if pos.x >= self.sizes.x || pos.y >= self.sizes.y || pos.x < 0 || pos.y < 0 {
            return Err(PositionOutOfGrid.into());
        } else {
            Ok(())
        }
    }

    /// Check for a column coordinate to be inside the grid
    pub fn check_grid_bounds_x(&self, x: i64) -> result::Result<()> {
        if x >= self.sizes.x || x < 0 {
            return Err(PositionOutOfGrid.into());
        } else {
            Ok(())
        }
    }

    /// Generate a new state adding a cell at the top of the column `x`
    pub fn next_state_x(&self, x: i64) -> result::Result<State> {
        self.check_grid_bounds_x(x)?;
        let mut state = self.clone();
        if let Some(col) =
        state.grid
            .get_mut(x as usize) {
            col.push(Cell {
                color: self.turn.clone(),
            });
            state.turn = state.turn.other();
            Ok(state)
        } else {
            Err(GridAccess.into())
        }
    }

    /// Returns a vector that contains the coordinates of each cell inserted in the grid
    pub fn all_cells(&self) -> Vec<V2<i64>> {
        self.grid
            .par_iter()
            .enumerate()
            .map(|(x, col)| {
                col
                    .par_iter()
                    .enumerate()
                    .map(|(y, _)| V2::new(x as i64, y as i64))
                    .collect::<Vec<_>>()
            })
            .flatten()
            .collect::<Vec<_>>()
    }

    /// Returns a vector that contains the coordinates of each cell on the higher position for each column
    pub fn top_cells(&self) -> Vec<V2<i64>> {
        self.grid
            .par_iter()
            .enumerate()
            .filter_map(|(x, col)| {
                let x = x as i64;
                let y = col.len() as i64 - 1;

                if 0 <= y {
                    Some(V2::new(x, y))
                } else {
                    None
                }
            })
            .collect()
    }

    /// Returns a vector that contains the coordinates of the lowest empty coordinate for each column
    pub fn valid_moves(&self) -> Vec<V2<i64>> {
        self.grid
            .par_iter()
            .enumerate()
            .filter_map(|(x, col)| {
                let x = x as i64;
                let y = col.len() as i64;

                if 0 <= y && y < self.sizes.y {
                    Some(V2::new(x, y))
                } else {
                    None
                }
            })
            .collect()
    }

    /// Returns the color of the cell at the given coordinate, if present
    pub fn cell_color(&self, pos: &V2<i64>) -> result::Result<Option<Color>> {
        debug_assert!(self.check_grid_bounds(pos).is_ok());

        Ok(self.grid
            .get(pos.x as usize)
            .and_then(|col| col.get(pos.y as usize))
            .map(|c| c.color.clone()))
    }

    /// Iterate origin and target and return each cell color, if present
    pub fn alignment(&self, origin: &V2<i64>, target: &V2<i64>) -> result::Result<Vec<Option<Color>>> {
        debug_assert!(self.check_grid_bounds(origin).is_ok());
        debug_assert!(self.check_grid_bounds(target).is_ok());

        V2Iter::new(origin, target)
            .map(|v| self.cell_color(&v))
            .collect()
    }

    /// Sequential cells of origin color
    pub fn alignment_extension_continuous(
        &self, origin: &V2<i64>, target: &V2<i64>)
        -> result::Result<(V2<i64>, usize)> {
        let c_origin = self.cell_color(&origin)?;

        Ok((
            origin.clone(),
            V2Iter::new(origin, target)
                .filter_map(|v| {
                    self.cell_color(&v).ok()
                        .map(|c| (v, c))
                })
                .take_while(|(_v, c)| { c == &c_origin })
                .count()
        ))
    }

    /// Sequential cells of origin color or valid position
    pub fn alignment_extension_heuristic(
        &self, origin: &V2<i64>, target: &V2<i64>)
        -> result::Result<(V2<i64>, usize)> {
        let c_origin = self.cell_color(&origin)?;
        let valid_pos = self.valid_moves();

        Ok((
            origin.clone(),
            V2Iter::new(origin, target)
                .filter_map(|v| {
                    self.cell_color(&v).ok()
                        .map(|c| (v, c))
                })
                .take_while(|(v, c)| {
                    c == &c_origin || valid_pos.contains(&v)
                })
                .count()
        ))
    }

    /// Sequential cells of origin color or valid position counting only placed cells
    pub fn alignment_extension_heuristic_present(
        &self, origin: &V2<i64>, target: &V2<i64>)
        -> result::Result<(V2<i64>, usize)> {
        let c_origin = self.cell_color(&origin)?;
        let valid_pos = self.valid_moves();

        Ok((
            origin.clone(),
            V2Iter::new(origin, target)
                .filter_map(|v| {
                    self.cell_color(&v).ok()
                        .map(|c| (v, c))
                })
                .take_while(|(v, c)| {
                    c == &c_origin || valid_pos.contains(&v)
                })
                .filter(|(_v, c)| c.is_some())
                .count()
        ))
    }

    /// Test all possible alignments starting from origin for `delta` steps and map the result using the given function
    pub fn max_alignment_f(&self, origin: &V2<i64>, delta: i64,
                           f: &dyn Fn(&V2<i64>, &V2<i64>) -> result::Result<(V2<i64>, usize)>,
    ) -> result::Result<Option<(V2<i64>, usize)>> {
        let low_y = max(origin.y - delta, 0);
        let low_x = max(origin.x - delta, 0);
        let high_x = min(origin.x + delta, self.sizes.x - 1);
        let high_y = min(origin.y + delta, self.sizes.y - 1);

        let ve = V2::new(high_x, origin.y);
        let vse = V2::new(high_x, low_y);
        let vs = V2::new(origin.x, low_y);
        let vsw = V2::new(low_x, low_y);
        let vw = V2::new(low_x, origin.y);
        let vnw = V2::new(low_x, high_y);
        let vn = V2::new(origin.x, high_y);
        let vne = V2::new(high_x, high_y);


        let res = vec![
            f(&origin, &ve)?, f(&origin, &vse)?, f(&origin, &vs)?, f(&origin, &vsw)?,
            f(&origin, &vw)?, f(&origin, &vnw)?, f(&origin, &vn)?, f(&origin, &vne)?
        ];

        Ok(res
            .par_iter()
            .cloned()
            .max_by_key(|(_v, r)| r.clone()))
    }

    /// Test all possible 4 cells alignments starting from origin counting only contiguous cells
    pub fn max_contiguous(&self, player: &Color) -> result::Result<Option<(V2<i64>, usize)>> {
        Ok(self.all_cells()
            .par_iter()
            .filter(|v| self
                .cell_color(v)
                .unwrap() == Some(player.clone()))
            .filter_map(|v| self
                .max_alignment_f(
                    v, 3,
                    &|v1, v2| self
                        .alignment_extension_continuous(v1, v2))
                .ok()
            )
            .filter_map(|r| r)
            .max_by_key(|(_v, r)| r.clone()))
    }

    /// Heuristic function
    ///
    /// Iterate over each cell, filter out all cells that are not of the player, we want to start the search
    /// from the player's cells only.
    ///
    /// Filter out all cells that can't obtain a 4 cell alignment.
    ///
    /// Calculate the max exact extension length from the remaining cells.
    pub fn max_heuristic(&self, player: &Color) -> result::Result<Option<(V2<i64>, Evaluation)>> {
        Ok(self
            .all_cells().par_iter()
            .filter(|v| self
                .cell_color(v).unwrap() == Some(player.clone()))
            .filter_map(|v| self
                .max_alignment_f(
                    v, 3,
                    &|v1, v2| self
                        .alignment_extension_heuristic(v1, v2))
                .ok()
            )
            .filter_map(|v| v)
            .filter(|(_v, r)| r >= &4)
            .filter_map(|(v, _)| self
                .max_alignment_f(
                    &v, 3,
                    &|v1, v2| self
                        .alignment_extension_heuristic_present(v1, v2))
                .ok()
            )
            .filter_map(|v| v)
            .map(|(v, r)| (v, match r {
                0 => Evaluation::Val(0),
                1 => Evaluation::Val(1),
                2 => Evaluation::Val(2),
                3 => Evaluation::Val(3),
                _ => Evaluation::Best
            }))
            .max_by_key(|(_v, r)| r.clone()))
    }

    pub fn winner(&self) -> Option<Color> {
        self
            .all_cells()
            .par_iter()
            .filter_map(|v| self
                .max_alignment_f(
                    v, 3,
                    &|v1, v2| self
                        .alignment_extension_continuous(v1, v2))
                .ok())
            .filter_map(|v| v)
            .max_by_key(|(_v, r)| r.clone())
            .and_then(|(v, r)| if r >= 4 {
                Some(v)
            } else {
                None
            })
            .and_then(|v| self.cell_color(&v).unwrap())
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Turn: {:?}", self.turn)?;
        writeln!(f, "Sizes: X = {}, Y = {}", self.sizes.x, self.sizes.y)?;

        for y in (0..self.sizes.y).rev() {
            let y = y as usize;
            write!(f, "{:4}\t", y)?;
            for x in 0..self.sizes.x {
                let x = x as usize;
                match &self.grid[x as usize].get(y) {
                    Some(c) => write!(f, "{}\t", match c.color {
                        Color::Red => "Red ",
                        Color::Blue => "Blue",
                    })?,
                    v => write!(f, "{:?}\t", v)?,
                }
            }
            writeln!(f, "")?;
        }
        write!(f, "{:>4}\t", "Y/X")?;
        for x in 0..self.sizes.x {
            write!(f, "{:4}\t", x)?
        }

        Ok(())
    }
}

/// Single game cell
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Cell {
    color: Color,
}

impl Cell {
    pub fn get_color(&self) -> &Color {
        &self.color
    }

    pub fn set_color(&mut self, c: Color) {
        self.color = c
    }
}

/// Player colors
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Color {
    Red,
    Blue,
}

impl Color {
    pub fn other(&self) -> Color {
        match &self {
            Red => Blue,
            Blue => Red,
        }
    }
}

#[cfg(test)]
mod tests {
    use log::debug;

    use crate::logs::init_logger_tests;

    use super::*;

    #[test]
    fn display() -> result::Result<()> {
        init_logger_tests();

        debug!("display");

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(0)?
            .next_state_x(3)?
            .next_state_x(4)?;

        assert_eq!(
            format!("{}", state),
            "Turn: Red
Sizes: X = 5, Y = 5
   4	None	None	None	None	None\t
   3	None	None	None	None	None\t
   2	None	None	None	None	None\t
   1	Blue	None	None	None	None\t
   0	Red 	Blue	Red 	Red 	Blue\t
 Y/X	   0	   1	   2	   3	   4\t");

        Ok(())
    }

    #[test]
    fn check_position() -> result::Result<()> {
        init_logger_tests();

        debug!("check_position");

        let state = State::new(&V2::new(10, 10));

        state.check_grid_bounds(&V2::new(0, 0))
            .expect("Should be inside grid");
        state.check_grid_bounds(&V2::new(0, 9))
            .expect("Should be inside grid");
        state.check_grid_bounds(&V2::new(9, 0))
            .expect("Should be inside grid");
        state.check_grid_bounds(&V2::new(9, 9))
            .expect("Should be inside grid");

        state.check_grid_bounds(&V2::new(10, 0))
            .expect_err("Should be outside grid");
        state.check_grid_bounds(&V2::new(0, 10))
            .expect_err("Should be outside grid");
        state.check_grid_bounds(&V2::new(10, 10))
            .expect_err("Should be outside grid");

        Ok(())
    }

    #[test]
    fn top_positions() -> result::Result<()> {
        init_logger_tests();

        debug!("top_positions");

        let state = State::new(&V2::new(10, 10))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(4)?
            .next_state_x(6)?
            .next_state_x(9)?;

        assert_eq!(vec![
            V2::new(0, 0), V2::new(1, 0),
            V2::new(2, 0), V2::new(4, 0),
            V2::new(6, 0), V2::new(9, 0)
        ], state.top_cells());

        Ok(())
    }

    #[test]
    fn valid_moves() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(10, 10))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(4)?
            .next_state_x(6)?
            .next_state_x(9)?;

        assert_eq!(vec![
            V2::new(0, 1), V2::new(1, 1),
            V2::new(2, 1), V2::new(3, 0),
            V2::new(4, 1), V2::new(5, 0),
            V2::new(6, 1), V2::new(7, 0),
            V2::new(8, 0), V2::new(9, 1)
        ], state.valid_moves());

        Ok(())
    }

    #[test]
    fn cell_color() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(10, 10))
            .next_state_x(0)?
            .next_state_x(1)?;

        assert_eq!(Some(Red), state.cell_color(&V2::new(0, 0))?);
        assert_eq!(Some(Blue), state.cell_color(&V2::new(1, 0))?);
        assert_eq!(None, state.cell_color(&V2::new(2, 0))?);

        Ok(())
    }

    #[test]
    fn alignment() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(10, 10))
            .next_state_x(0)?
            .next_state_x(1)?;

        assert_eq!(
            vec![Some(Red), Some(Blue), None, None],
            state.alignment(&V2::new(0, 0), &V2::new(3, 0))?);
        assert_eq!(
            vec![Some(Red), None, None, None],
            state.alignment(&V2::new(0, 0), &V2::new(0, 3))?);
        assert_eq!(
            vec![Some(Red), None, None, None],
            state.alignment(&V2::new(0, 0), &V2::new(3, 3))?);
        Ok(())
    }

    #[test]
    fn alignment_extension_continuous() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(1)?
            .next_state_x(1)?;

        assert_eq!(
            1,
            state.alignment_extension_continuous(&V2::new(0, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            1,
            state.alignment_extension_continuous(&V2::new(0, 0), &V2::new(0, 3))?.1);
        assert_eq!(
            1,
            state.alignment_extension_continuous(&V2::new(1, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            1,
            state.alignment_extension_continuous(&V2::new(1, 0), &V2::new(1, 3))?.1);
        assert_eq!(
            2,
            state.alignment_extension_continuous(&V2::new(0, 0), &V2::new(3, 3))?.1);
        Ok(())
    }

    #[test]
    fn alignment_extension_heuristic() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(1)?;

        assert_eq!(
            1,
            state.alignment_extension_heuristic(&V2::new(0, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            3,
            state.alignment_extension_heuristic(&V2::new(0, 0), &V2::new(0, 3))?.1);
        assert_eq!(
            3,
            state.alignment_extension_heuristic(&V2::new(1, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            3,
            state.alignment_extension_heuristic(&V2::new(1, 0), &V2::new(1, 3))?.1);
        assert_eq!(
            1,
            state.alignment_extension_heuristic(&V2::new(0, 0), &V2::new(3, 3))?.1);

        let state = State::new(&V2::new(4, 4))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(0)?
            .next_state_x(3)?
            .next_state_x(3)?;

        assert_eq!(
            4,
            state.alignment_extension_heuristic(&V2::new(0, 1), &V2::new(3, 1))?.1);
        assert_eq!(
            3,
            state.alignment_extension_heuristic(&V2::new(1, 0), &V2::new(3, 0))?.1);

        Ok(())
    }

    #[test]
    fn alignment_extension_heuristic_present() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(1)?;


        assert_eq!(
            1,
            state.alignment_extension_heuristic_present(&V2::new(0, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            2,
            state.alignment_extension_heuristic_present(&V2::new(0, 0), &V2::new(0, 3))?.1);
        assert_eq!(
            1,
            state.alignment_extension_heuristic_present(&V2::new(1, 0), &V2::new(3, 0))?.1);
        assert_eq!(
            2,
            state.alignment_extension_heuristic_present(&V2::new(1, 0), &V2::new(1, 3))?.1);
        assert_eq!(
            1,
            state.alignment_extension_heuristic_present(&V2::new(0, 0), &V2::new(3, 3))?.1);

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(0)?
            .next_state_x(3)?
            .next_state_x(3)?;

        assert_eq!(
            3,
            state.alignment_extension_heuristic_present(&V2::new(0, 1), &V2::new(3, 1))?.1);
        assert_eq!(
            3,
            state.alignment_extension_heuristic_present(&V2::new(1, 0), &V2::new(3, 0))?.1);

        Ok(())
    }

    #[test]
    fn max_alignment_f() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(4)?
            .next_state_x(0)?;

        assert_eq!(
            2,
            state.max_alignment_f(
                &V2::new(0, 0), 4,
                &|v1, v2| state.alignment_extension_continuous(v1, v2))?.unwrap().1);
        assert_eq!(
            2,
            state.max_alignment_f(
                &V2::new(0, 1), 4,
                &|v1, v2| state.alignment_extension_continuous(v1, v2))?.unwrap().1);
        assert_eq!(
            2,
            state.max_alignment_f(
                &V2::new(1, 1), 4,
                &|v1, v2| state.alignment_extension_continuous(v1, v2))?.unwrap().1);

        assert_eq!(
            2,
            state.max_alignment_f(
                &V2::new(0, 0), 4,
                &|v1, v2| state.alignment_extension_heuristic(v1, v2))?.unwrap().1);
        assert_eq!(
            2,
            state.max_alignment_f(
                &V2::new(1, 1), 4,
                &|v1, v2| state.alignment_extension_heuristic(v1, v2))?.unwrap().1);
        assert_eq!(
            3,
            state.max_alignment_f(
                &V2::new(0, 2), 4,
                &|v1, v2| state.alignment_extension_heuristic(v1, v2))?.unwrap().1);
        Ok(())
    }

    #[test]
    fn max_heuristic() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(0)?;

        assert_eq!(None, state.max_heuristic(&Red)?);
        assert_eq!(Some((V2::new(1, 0), Evaluation::Val(2))), state.max_heuristic(&Blue)?);

        Ok(())
    }

    #[test]
    fn max_continuous() -> result::Result<()> {
        init_logger_tests();

        let state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(4)?
            .next_state_x(0)?
            .next_state_x(0)?
            .next_state_x(2)?;

        assert_eq!(
            Some((V2::new(0, 1), 2)),
            state.max_contiguous(&Red)?);
        assert_eq!(
            Some((V2::new(2, 0), 3)),
            state.max_contiguous(&Blue)?);

        Ok(())
    }

    #[test]
    fn winner() -> result::Result<()> {
        init_logger_tests();

        let mut state = State::new(&V2::new(5, 5))
            .next_state_x(0)?
            .next_state_x(1)?
            .next_state_x(1)?
            .next_state_x(2)?
            .next_state_x(1)?
            .next_state_x(3)?;

        assert_eq!(None, state.winner());

        state = state
            .next_state_x(0)?
            .next_state_x(4)?;

        assert_eq!(Some(Blue), state.winner());

        Ok(())
    }
}



