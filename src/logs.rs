/// Start logger for standard execution
pub fn init_logger() {
    env_logger::try_init().unwrap_or(());
}

/// Start logger for test execution
pub fn init_logger_tests() {
    env_logger::builder().is_test(true).try_init().unwrap_or(());
}