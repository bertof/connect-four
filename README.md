# Forza 4

[![pipeline status](https://gitlab.com/bertof/connect-four/badges/master/pipeline.svg)](https://gitlab.com/bertof/connect-four/commits/master)

A Rust implementation of Connect 4 using an Alpha-Beta Pruning based AI

## Build instruction

The current configuration works on GNU/Linux and uses the OpenGL backend of [Piston](crates.io/crates/piston).
To build the game use the command

```sh
cargo build --release
```

To run the game use the command

```sh
cargo run --release
```

## CLI options

Run the game with the `--help` argument to see the help menu:

```sh
cargo run --release -- --help
```

The search algorithm of the AI can be choosen with the `-a` argument between `minimax` and the default `alpha_beta`.
The depth of search can be choosen with the `-d` argument and defaults to `4`.
